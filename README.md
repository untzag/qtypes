# qtypes

[![PyPI](https://img.shields.io/pypi/v/qtypes)](https://pypi.org/project/qtypes)
[![Conda](https://img.shields.io/conda/vn/conda-forge/qtypes)](https://anaconda.org/conda-forge/qtypes)
[![black](https://img.shields.io/badge/code--style-black-black)](https://black.readthedocs.io/)
[![log](https://img.shields.io/badge/change-log-informational)](https://gitlab.com/yaq/qtypes/-/blob/master/CHANGELOG.md)

Build qt graphical user interfaces out of simple type objects.

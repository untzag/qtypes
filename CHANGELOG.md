# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/).

## [Unreleased]

### Fixed
- strings disabled properly at startup

## [0.2.0]

### Added
- filepath support

### Changed
- moved to get/set instead of read/write
- migrate to flit

### Fixed
- spacing issues with scroll area

## [0.1.0]

### Added
- initial release

[Unreleased]: https://gitlab.com/yaq/qtypes/-/compare/v0.2.0...master
[0.2.0]: https://gitlab.com/yaq/qtypes/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/yaq/qtypes/-/tags/v0.1.0
